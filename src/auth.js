import axios from "axios";

export const instance = axios.create({
  baseURL: REACT_APP_BACKEND_API_URL_USERS_ORGANIZATIONS,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use(
  (config) => {
    const token = AuthToken.get("access_token");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

localStorage.getItem("access_token");

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (err) => {
    const originalConfig = err.config;
    if (err.response) {
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;
        try {
          const {
            data: { access },
          } = await instance.post(
            `${REACT_APP_BACKEND_API_URL_USERS_ORGANIZATIONS}login/refresh/`,
            {
              refresh: refreshToken,
            }
          );
          localStorage.setItem("access_token", access);
          return instance(originalConfig);
        } catch (_error) {
          store.dispatch(push(`/login`));
          localStorage.removeItem("access_token");
          localStorage.removeItem("refresh_token");
          console.log("_error", _error.response);
          store.dispatch(setLoginFailure(_error.response.status));
          return Promise.reject("");
        }
      }
    }
    return Promise.reject(err);
  }
);
const refreshToken = localStorage.getItem("refresh_token");
