import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import { Objectives } from "./Components/Objectives";
import { SignIn } from "./Components/SignIn";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SignIn />} />
        <Route path="/objectives" element={<Objectives />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
