import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./Auth/authSlice";
import objectivesSlice from "./Objectives/objectivesSlice";

export const store = configureStore({
  reducer: {
    auth: authSlice,
    objectives: objectivesSlice,
  },
});
