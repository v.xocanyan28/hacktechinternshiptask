import { setObjectives } from "./objectivesSlice";

const { REACT_APP_BACKEND_API_URL } = process.env;

export const getObjectives = (access) => async (dispatch) => {
  const response = await fetch(`${REACT_APP_BACKEND_API_URL}okr/objectives/`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${access}`,
    },
  });
  const result = await response.json();
  dispatch(setObjectives(result.results));
};
