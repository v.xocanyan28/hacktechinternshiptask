import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  objectives: [],
};

const objectivesSlice = createSlice({
  name: "objectives",
  initialState,
  reducers: {
    setObjectives: (state, action) => {
      state.objectives = action.payload;
    },
  },
});

export const { setObjectives } = objectivesSlice.actions;
export default objectivesSlice.reducer;
