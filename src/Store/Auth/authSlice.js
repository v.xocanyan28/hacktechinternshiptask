import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loginSuccess: null,
};
const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setLoginSuccess: (state, action) => {
      state.loginSuccess = action.payload;
    },
  },
});
export const { setLoginSuccess } = authSlice.actions;
export default authSlice.reducer;
