import { setLoginSuccess } from "./authSlice";

const { REACT_APP_BACKEND_API_URL } = process.env;

export const signInUser = (data, navigate) => async (dispatch) => {
  const response = await fetch(`${REACT_APP_BACKEND_API_URL}accounts/login/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  const result = await response.json();
  localStorage.setItem("access_token", result.access);
  localStorage.setItem("refresh_token", result.refresh);
  if (response.status === 200) {
    dispatch(setLoginSuccess(true));
    navigate("/objectives");
  }

  return result;
};

export const LogOutUser = (refresh, access, navigate) => async (dispatch) => {
  const response = await fetch(`${REACT_APP_BACKEND_API_URL}accounts/logout/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access}`,
    },
    body: JSON.stringify({
      refresh: refresh,
    }),
  });

  localStorage.removeItem("access_token");
  localStorage.removeItem("refresh_token");
  if (response.status === 205) {
    dispatch(setLoginSuccess(false));
    navigate("/");
  }
};
