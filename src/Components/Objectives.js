import { getObjectives } from "../Store/Objectives/actions";
import { ObjectiveItem } from "./ObjectiveItem";
import { LogOutUser } from "../Store/Auth/actions";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import "./Styles/objectives.css";

export const Objectives = () => {
  const dispatch = useDispatch();
  const { objectives } = useSelector((state) => state.objectives);
  const navigate = useNavigate();
  dispatch(getObjectives(localStorage.getItem("access_token")))
  return (
    <div>
      <button
        className="logOut"
        type="submit"
        onClick={() =>
          dispatch(
            LogOutUser(
              localStorage.getItem("refresh_token"),
              localStorage.getItem("access_token"),
              navigate
            )
          )
        }
      >
        LogOut
      </button>
      <div>{<ObjectiveItem objectives={objectives} />}</div>
    </div>
  );
};
