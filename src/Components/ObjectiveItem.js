import "./Styles/objectiveItem.scss";

export const ObjectiveItem = ({ objectives }) => {
  const objectivesTree = objectives.map((objective,index) => {
    if (objective.children.length > 0) {
      return (
        <details key={index}>
          <summary>
            <i className="fa fa-circle"></i>
            <div className="selector"></div>
            {objective.name}
          </summary>
          {<ObjectiveItem key={objective.id} objectives={objective.children} />}
        </details>
      );
    }
    return (
      <details key={index}>
        <summary>
          <i className="fa fa-circle"></i>
          <div className="selector"></div>
          {objective.name}
        </summary>
      </details>
    );
  });
  return (
    <div className="container">
      <h2>My Objectives</h2>
      {objectivesTree}
    </div>
  );
};
