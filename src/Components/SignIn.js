import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { signInUser } from "../Store/Auth/actions";
import "./Styles/signIn.css";

export const SignIn = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [signInInfo, setSignInInfo] = useState({
    email: "",
    password: "",
  });

  return (
    <div className="signIn-form">
      <h2>Sign In</h2>
      <input
        type="text"
        placeholder="Email"
        value={signInInfo.email}
        onChange={(e) =>
          setSignInInfo({
            ...signInInfo,
            email: e.target.value,
          })
        }
      />
      <br />
      <input
        type="password"
        placeholder="Password"
        value={signInInfo.password}
        onChange={(e) =>
          setSignInInfo({
            ...signInInfo,
            password: e.target.value,
          })
        }
      />
      <br />
      <button
        className="submit-button"
        type="submit"
        onClick={() => dispatch(signInUser(signInInfo, navigate))}
      >
        SignIn
      </button>
      <br />
    </div>
  );
};
